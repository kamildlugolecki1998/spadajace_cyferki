﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Wartosc : MonoBehaviour
{

    //  [SerializeField] Text txtWartosc;
    [SerializeField] Sprite spriteSplash;
    [SerializeField] Sprite[] sprityCyferek;
    GameObject txtLiczba;
    GameManager gm;
    RectTransform rt;
    Animator anim;
    Parzystosc parz;
    SoundManager soundM;
    float predkosOpadania = 100f;
    int wybranaLiczba;
    
    // Start is called before the first frame update
    void Start()
    {
        soundM = GameObject.FindGameObjectWithTag("soundManager").GetComponent<SoundManager>();
        anim = GetComponent<Animator>();
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        parz = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Parzystosc>();
        rt = GetComponent<RectTransform>();
        PrzypiszWartoscCyferki();

    }

    // Update is called once per frame
    void Update()
    {
        SpadajWDol();
        if (rt.anchoredPosition.y < -400)
        {
            UsunCyferke(0f);
        }
    }

     void PrzypiszWartoscCyferki()
    {
        int losowaLiczba = Random.Range(1, 7);
        wybranaLiczba = losowaLiczba;
        GetComponent<Image>().sprite = sprityCyferek[losowaLiczba - 1];
       
        
    }

    void AktywujSplasha()
    {
        gameObject.GetComponent<Image>().sprite = spriteSplash;
    }

    public void AkcjaPoKliknieciu()
    {
        
        gm.DodajPunkty(wybranaLiczba);
        parz.PobierzKliknietaCyferke(wybranaLiczba);
        AktywujSplasha();
        anim.SetTrigger("Znikanie");
        ZmienKolorPrzyciusku();
        GetComponent<Button>().interactable = false;
        soundM.OdtworzDzwiek(0);
        UsunCyferke(0.5f);
    }

    

    void SpadajWDol()
    {
        rt.anchoredPosition -= new Vector2(0f, 1f) * predkosOpadania * Time.deltaTime;
    }
    void UsunCyferke(float time)
    {
        Destroy(gameObject, time);
    }

    void ZmienKolorPrzyciusku()
    {
        ColorBlock blokKolorow = GetComponent<Button>().colors;
        switch(wybranaLiczba)
        {
            case 1 : blokKolorow.disabledColor = Color.red;
                break;
            case 2:
                blokKolorow.disabledColor = Color.yellow;
                break;
            case 3:
                blokKolorow.disabledColor = Color.green;
                break;
            case 4:
                blokKolorow.disabledColor = new Color(0.92f,0.41f,0f,1f);
                break;
            case 5:
                blokKolorow.disabledColor = new Color(1f,0f,1f,1f);
                break;
            case 6:
                blokKolorow.disabledColor = Color.blue;
                break;

            default: blokKolorow.disabledColor = Color.cyan;
                break;
        }
        
        GetComponent<Button>().colors = blokKolorow;
    }
}
