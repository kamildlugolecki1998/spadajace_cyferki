﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Parzystosc : MonoBehaviour
{
    [SerializeField] Text txtWarunek;
  
    bool czyNasteppnaCyfraMaBycParzysta;
    int kliknietaCyferka;
    bool czyKliknietaLiczbaJestParzysta;
    string infOParzystosci;
    // Start is called before the first frame update
    void Start()
    {
        LosujWarunek();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


   public void LosujWarunek()
    {
        if (GetComponent<GameManager>().PobierzIlePunktowZostalo() == 1)
        {
            czyNasteppnaCyfraMaBycParzysta = false;
           infOParzystosci = "Nieparzysta";
            txtWarunek.color = Color.blue;
        }
        else
        {

            int losowaLiczbe = Random.Range(0, 2);

            if (losowaLiczbe == 0)
            {
                czyNasteppnaCyfraMaBycParzysta = true;
                infOParzystosci = "Parzysta";
                txtWarunek.color = Color.green;
            }else
            {
                czyNasteppnaCyfraMaBycParzysta = false;
                infOParzystosci = "Nieparzysta";
                txtWarunek.color = Color.blue;
            }
        }
        txtWarunek.text = infOParzystosci;
    }

    public void PobierzKliknietaCyferke(int cyferka)
    {
        kliknietaCyferka = cyferka;
        SprawdzKlikniecie();
    }

    void SprawdzKlikniecie()
    {
        if (kliknietaCyferka % 2 == 0)
        {
            czyKliknietaLiczbaJestParzysta = true;
        }
        else
        {
            czyKliknietaLiczbaJestParzysta = false;
        }

        if (czyKliknietaLiczbaJestParzysta == czyNasteppnaCyfraMaBycParzysta)
        {
            LosujWarunek();
        }else
        {
            GetComponent<GameManager>().GameOver();
        }
    }
}
