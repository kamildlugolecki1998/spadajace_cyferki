﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SoundManager : MonoBehaviour
{
    AudioSource audios;
    [SerializeField] AudioClip[] plikiDzwiekowe;
    private void Awake()
    {
        GameObject[] sm = GameObject.FindGameObjectsWithTag("soundManager");
        if (sm.Length > 1 )
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        audios = GetComponent<AudioSource>();
    }

   public void OdtworzDzwiek(int nrPliku)
    {
        audios.clip = plikiDzwiekowe[nrPliku];
        audios.Play();
    }
}
